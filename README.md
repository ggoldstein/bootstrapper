# Bootstrap scripts for new Linux machines
To use:

* Install git and ansible.
* Clone this repository.
* Check the settings in top-level playbooks before running them. You'll usually need an account that can escalate privileges.
* If you're not currently running as the user you want to work as, run the user.yaml playbook to take care of this.
* Run specific playbooks to set things up, or run everything.yaml to set everything up.

These playbooks will usually be used to enhance the machine you're working on, but they can target other machines to configure them.

## user.yaml
This makes sure a specific user exists and has passwordless escalation permissions. It can also disable other users by setting their shell to nologin.

## everything.yaml
This runs all following playbooks in sequence.

## git.yaml
This installs git onto the target system(s) and configures the user as per the variables in the playbook.

## crontab.yaml
This adds crontab entries that a machine might want.

## docker.yaml
This installs, enables and starts docker.

## oc.yaml
This installs the OpenShift Command Line Tools for the current user
